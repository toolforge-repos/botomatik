#! /usr/bin/python
# -*- coding: utf-8 -*-

import datetime
import pywikibot

debug = False

def main():
	# Configuration
	modeles_mensuels = {
		# 'NavigPM': 'Utilisateur:Botomatik/',
		'NavigQM': 'Wiktionnaire:Questions sur les mots/',
		'NavigQT': 'Wiktionnaire:Questions techniques/',
		'NavigPM': 'Wiktionnaire:Proposer un mot/',
		'NavigPPS': 'Wiktionnaire:Pages proposées à la suppression/',
		'NavigW': 'Wiktionnaire:Wikidémie/',
		'NavigDA': 'Wiktionnaire:Demandes aux administrateurs/',
	}
	modeles_annuels = {
		'NavigBA': 'Wiktionnaire:Bulletin des administrateurs/',
		'NavigBP': 'Wiktionnaire:Bulletin de la patrouille/',
		'NavigBOT': 'Wiktionnaire:Bots/Requêtes/Archives/',
		'Wiktionnaire:Gestion des modèles/En-tête': 'Wiktionnaire:Gestion des modèles/',
		'Wiktionnaire:Gestion des catégories/En-tête': 'Wiktionnaire:Gestion des catégories/',
	}
	resume = 'Initialisation de la page'

	mois = ['janvier','février','mars','avril','mai','juin','juillet','août','septembre','octobre','novembre','décembre']
	date = datetime.datetime.utcnow()
	month = date.date().month
	month_name = mois[month-1]
	year = date.date().year
	
	site = pywikibot.Site()

	def _save(page, contenu):
		if not debug:
			page.put(contenu, resume)
		else:
			pywikibot.output(page.title() + ' : ' + contenu)

	for modele in modeles_mensuels:
		title = modeles_mensuels[modele] + '%s %s' % (month_name, year)
		page = pywikibot.Page(site, title)
		if page.exists():
			pywikibot.output(f'Page {title} already exists. Skipping.')
			continue
		contenu = '<noinclude>{{%s|%s|%s}}</noinclude>' % (modele, year, month)
		_save(page, contenu)

	if month_name == 'janvier':
		for modele in modeles_annuels:
			title = modeles_annuels[modele] + '%s' % year
			page = pywikibot.Page(site, title)
			if page.exists():
				pywikibot.output(f'Page {title} already exists. Skipping.')
				continue
			contenu = '<noinclude>{{%s|%s}}</noinclude>' % (modele, year)
			_save(page, contenu)

if __name__ == '__main__':
	try:
		main()
	finally:
		pywikibot.stopme()
