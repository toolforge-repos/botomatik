#! /usr/bin/python3
# -*- coding: utf-8 -*-
import os

import pywikibot


HOME = os.environ['HOME']  # in Toolforge: /data/project/<toolname>

project = 'frwiktionary'
dumpPath = f"http://dumps.wikimedia.org/{project}/latest/"
latestDumpDateFile = os.path.join(HOME, "new_dump_timestamp")
previousDumpDateFile = os.path.join(HOME, "old_dump_timestamp")
alert_page_title = "Wiktionnaire:Questions techniques/journaux"
wikitext_to_add = f"Le dernier dump vient d'être publié ([{dumpPath} le consulter], [{dumpPath}{project}-latest-pages-articles.xml.bz2 télécharger le fichier ''pages-articles.xml.bz2'']).\n\nMessage déposé par ~~~ le ~~~~~"

debug = False

def main():
    global wikitext_to_add
    with open(previousDumpDateFile, 'r', encoding=pywikibot.config.textfile_encoding) as f:
        old_timestamp = f.readline(8)
    if debug:
        pywikibot.output("Date de l'ancien dump : %s" % old_timestamp)
    with open(latestDumpDateFile, 'r', encoding=pywikibot.config.textfile_encoding) as f:
        new_timestamp = f.readline(8)
    if debug:
        pywikibot.output("Date du nouveau dump : %s" % new_timestamp)
    if int(new_timestamp) > int(old_timestamp):
        year = new_timestamp[:4]
        month = new_timestamp[4:6]
        day = new_timestamp[6:8]
        title = f"Dernier dump publié le {day}/{month}/{year}"
        alert_page = pywikibot.Page(pywikibot.Site(), alert_page_title)
        current_wikitext = alert_page.text
        wikitext_to_add = f"== {title} ==\n\n{wikitext_to_add}"
        if not debug:
            alert_page.text = current_wikitext + "\n\n" + wikitext_to_add
            alert_page.save(summary=f"/* {title} */ nouvelle section", minor=False)
            with open(previousDumpDateFile, 'w', encoding=pywikibot.config.textfile_encoding) as f:
                f.write(new_timestamp)
        else:
            pywikibot.output(wikitext_to_add)
    elif debug:
        pywikibot.output("Pas de nouveau dump.")

if __name__ == '__main__':
    try:
        main()
    finally:
        pywikibot.stopme()
